package controller;

import java.util.Collection;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.AlmacenService;
import domain.Almacen;

@Controller
public class AlmacenController {
	
	@Autowired
	AlmacenService almacenService;

	@RequestMapping(value = "/almacen", method = RequestMethod.POST)
	String saveAlmacen(@ModelAttribute Almacen almacen, ModelMap model) {
		System.out.println("Saving: " + almacen.getId());
		almacenService.save(almacen);
		return showAlmacen(almacen.getId(), model);
	}
	@RequestMapping(value = "/add-almacen", method = RequestMethod.GET)
	String addNewAlmacen(@RequestParam(required = false) Long id, ModelMap model) {
		Almacen almacen = id == null ? new Almacen() : almacenService.get(id);
		model.addAttribute("almacen", almacen);
		return "add-almacen";
	}

	@RequestMapping(value = "/almacen", method = RequestMethod.GET)
	String showAlmacen(@RequestParam(required = false) Long id_almacen, ModelMap model) {
		if (id_almacen != null) {
			Almacen almacen = almacenService.get(id_almacen);
			model.addAttribute("almacen", almacen);
			return "almacen";
		} else {
			Collection<Almacen> almacenes = almacenService.getAll();
			model.addAttribute("almacen", almacenes);
			return "almacen";
		}
	}

}