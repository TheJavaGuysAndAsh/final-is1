package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import domain.Usuario;
import service.LoginService;
import form.LoginForm;
import form.RegistroVentaForm;
import controller.RegistroVentaController;
@Controller
public class LoginController {

	@Autowired
	LoginService loginService;
	
	@Autowired
	RegistroVentaController registroventacontroller;
	
	RegistroVentaForm registroventaform;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	String login(@ModelAttribute LoginForm login, ModelMap model) {
		Usuario usuario = loginService.login(login.getUsername(), login.getPassword());
		if(usuario !=null)
		{
			if(usuario.getTipo() == 1)
				return registroventacontroller.showProducts(registroventaform, model,usuario);
			if(usuario.getTipo() == 2)
				return "add-producto";
			if(usuario.getTipo() == 3)
				return "admin";
		}
		return "login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	String showLogin(@ModelAttribute LoginForm login, ModelMap model) {
		return "login";
	}
}
