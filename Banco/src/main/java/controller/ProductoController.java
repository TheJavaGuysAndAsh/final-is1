package controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.ProductoService;
import domain.Producto;
import form.AddProductForm;
import form.LoginForm;

@Controller
public class ProductoController {
		
	@Autowired
	ProductoService productoService;
	@RequestMapping(value = "/add-producto", method = RequestMethod.POST)
	String addProducto(@ModelAttribute AddProductForm addproduct, ModelMap model) {
		productoService.add(addproduct.getnombreProducto(),addproduct.getTipo(),addproduct.getClasificacion(),addproduct.getCantidad());
		return "add-producto";
	}

	@RequestMapping(value = "/add-producto", method = RequestMethod.GET)
	String showProducto(@ModelAttribute AddProductForm addproductform, ModelMap model) {
		return "add-producto";
	}

}
