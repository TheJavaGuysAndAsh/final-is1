package controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.ProveedorService;
import domain.Proveedor;
import form.RegistroProveedorForm;
import service.ProductoService;

@Controller
public class ProveedorController {
	
	@Autowired
	ProveedorService proveedorService;
	
	@Autowired
	ProductoService productoService;
	
	@RequestMapping(value = "/add-proveedor", method = RequestMethod.POST)
	String saveProveedor(RegistroProveedorForm regproveedorform, ModelMap model) {
		proveedorService.saveProveedor(regproveedorform.getnombreProducto(),regproveedorform.getContacto(),regproveedorform.getClasificacion(),regproveedorform.getDireccion(),regproveedorform.getProductoId());
		return "add-proveedor";
	}
	
	@RequestMapping(value = "/add-proveedor", method = RequestMethod.GET)
	String showRegProveedor(RegistroProveedorForm regproveedorform, ModelMap model) {
		model.addAttribute("productos",productoService.getAll());
		return "add-proveedor";
	}

}
