package controller;

import java.util.Collection;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import form.AddProductForm;
import form.RegistroVentaForm;
import domain.RegistroVenta;
import domain.Producto;
import domain.Usuario;
import service.RegistroVentaService;
import service.ProductoService;

@Controller
public class RegistroVentaController {
	@Autowired
	RegistroVentaService registroVentaService;
	
	@Autowired
	ProductoService productoService;

	@RequestMapping(value = "/registrar-venta", method = RequestMethod.POST)
	String addProducto(@ModelAttribute RegistroVentaForm ventaform, ModelMap model) {
		registroVentaService.saveVenta(ventaform.getUsuarioId(),ventaform.getProductoId(),ventaform.getCantidad());
		return "add-producto";
	}
	
	@RequestMapping(value = "/registrar-venta", method = RequestMethod.GET)
	String showProducts(@ModelAttribute RegistroVentaForm registroventaform,ModelMap model,Usuario usuario) {
		Collection<Producto> productos = productoService.getAll();
		model.addAttribute("usuario",usuario);
		model.addAttribute("productos", productos);
		return "registrar-venta";
	}
}