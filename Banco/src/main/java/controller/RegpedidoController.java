package controller;

import java.util.Collection;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.RegpedidoService;
import service.ProductoService;
import service.ProveedorService;
import form.RegistroPedidoForm;
import domain.Reg_pedido;

@Controller
public class RegpedidoController {
	
	@Autowired
	RegpedidoService regpedidoService;

	@Autowired
	ProductoService productoService;
	
	@Autowired
	ProveedorService proveedorService;
	
	@RequestMapping(value = "/reg_pedido", method = RequestMethod.POST)
	String savereg_pedido(@ModelAttribute Reg_pedido regpedido, ModelMap model) {
		System.out.println("Saving: " + regpedido.getId());
		regpedidoService.save(regpedido);
		return "home";
	}
	
	@RequestMapping(value = "/registrar-pedido", method = RequestMethod.GET)
	String showRegPedido(RegistroPedidoForm regpedidoform, ModelMap model) {
		model.addAttribute("productos",productoService.getAll());
		model.addAttribute("proveedores",proveedorService.getAll());
		return "registrar-pedido";
	}
}