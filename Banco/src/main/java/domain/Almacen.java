package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Almacen", indexes = { @Index(columnList = "id") })
public class Almacen implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "account_id_generator", sequenceName = "account_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_id_generator")
	private Long id;

	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String name;

	@Column(nullable = false)
	private String location;

	public Almacen() {
	}

	public Almacen(String name, String location) {
		this.name = name;
		this.location = location;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setname(String name) {
		this.name = name;
	}

}
