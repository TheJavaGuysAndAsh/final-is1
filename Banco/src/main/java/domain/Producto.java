package domain;

import java.sql.Date;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Producto implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "Producto_id_generator", sequenceName = "Producto_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Producto_id_generator")
	private Long id_producto;
	
	@ManyToMany
    @JoinTable(name="producto_proveedor")
	private Collection<Proveedor> proveedores;
	
	
	
	@Column(length = 64)
	private String nombreProducto;
	
	@Column(length = 10)
	private String tipo;
	
	@Column(length = 1)
	private String clasificacion;

	@Column(length = 7)
	private Long cantidad;
	
	@Column(length = 7)
	private Long costo;
	
	@Column(length = 7)
	private String estanteria;
	
	@Column(length = 7)
	private Long numeroFila;
	
	@Column(length = 7)
	private Long numeroColumna;
	
	@Column(nullable = true)
	private Date ultimaVenta;
	
	@Column(nullable = true)
	private Boolean stockAlert;
	
	public Producto()
	{
		
	}
	
	public Producto(String nombreProducto,String tipo,String clasificacion,Long cantidad) {
		this.nombreProducto = nombreProducto;
		this.tipo = tipo;
		this.clasificacion = clasificacion;
		this.cantidad = cantidad;
	}
	
	@Override
	public Long getId() {
		return id_producto;
	}

	@Override
	public void setId(Long id) {
		this.id_producto = id;
	}

	public String getNombre() {
		return nombreProducto;
	}

	public void setNombre(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}
	
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setEstanteria(String estanteria) {
		this.estanteria = estanteria;
	}

	public String getEstanteria() {
		return estanteria;
	}
	public void setNumeroFila(Long numeroFila) {
		this.numeroFila = numeroFila;
	}

	public Long getNumeroFila() {
		return numeroFila;
	}
	public void setNumeroColumna(Long numeroColumna) {
		this.numeroColumna = numeroColumna;
	}

	public Long getNumeroColumna() {
		return numeroColumna;
	}
	public void setCosto(Long costo) {
		this.costo = costo;
	}

	public Long getCosto() {
		return costo;
	}
	public void sumCantidad(Long cantidad) {
		this.cantidad = this.cantidad + cantidad;
	}
	
	public void restCantidad(Long cantidad) {
		this.cantidad = this.cantidad - cantidad;
	}
	
	public Date getultimaVenta() {
		return ultimaVenta;
	}
	
	public Boolean getAlert() {
		return stockAlert;
	}
}

