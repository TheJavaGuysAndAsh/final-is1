package domain;

import java.sql.Date;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Proveedor implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "proveedor_id_generator", sequenceName = "proveedor_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "proveedor_id_generator")
	private Long id_proveedor;

	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String nombre_proveedor;

	@ManyToMany(mappedBy = "proveedores")
	private Collection<Producto> productos;
	
	@Column()
	private Long id_producto;
	
	@Column(length = 64)
	private String direccion;
	
	@Column()
	private Long contacto;



	@Override
	public Long getId() {
		return id_proveedor;
	}

	@Override
	public void setId(Long proveedor_id) {
		this.id_proveedor = proveedor_id;
	}
	public String getnombre_proveedor() {
		return nombre_proveedor;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public void setContacto(Long contacto) {
		this.contacto = contacto;
	}
	
	
	public String getDireccion() {
		return direccion;
	}

	public void setnombre_proveedor(String nombre_proveedor) {
		this.nombre_proveedor = nombre_proveedor;
	}

	public Long getid_producto(){
		return id_producto;
	}
	
	public Long getContacto(){
		return contacto;
	}
	

}