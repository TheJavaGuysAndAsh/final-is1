package domain;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
public class Reg_pedido implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "Reg_id_generator", sequenceName = "Reg__id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Reg_id_generator")
	private Long id_Reg;

	@ManyToOne
	private Usuario id_usuarios;

	@ManyToOne
	private Proveedor id_proveedores;

	@ManyToOne
	private Producto id_productos;

	@Column()
	private boolean estado;


	@Column(length = 7)
	private Long Cantidad;

	@Column(nullable = false)
	private Date Fecha_pedido;

	@Override
	public Long getId() {
		return id_Reg;
	}

	@Override
	public void setId(Long id_Reg) {
		this.id_Reg = id_Reg;
	}

	public Long getCantidad() {
		return Cantidad;
	}

	public Date Fecha_pedido() {
		return Fecha_pedido;
	}

	public void setFecha_pedido(Date Fecha_pedido) {
		this.Fecha_pedido = Fecha_pedido;
	}
	public boolean get_estado(){
		return estado;
	}
}
