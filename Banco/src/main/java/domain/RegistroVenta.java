package domain;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
public class RegistroVenta implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "RegVenta_id_generator", sequenceName = "RegVenta_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RegVenta_id_generator")
	private Long regVentaId;
	
	@Column()
	private Long usuarioId;
	
	@Column()
	private Long productoId;
	
	@Column(length = 7)
	private Long cantidad;
	
	@Column(nullable = false)
	private Date fechaVenta = new Date();
	
	@Override
	public Long getId() {
		return regVentaId;
	}

	@Override
	public void setId(Long regVentaId) {
		this.regVentaId = regVentaId;
	}

	public Long getCantidad() {
		return cantidad;
	}
	
	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}
	
	public Date fechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}
	
	public Long getusuario_id(){
		return usuarioId;
	}
	public Long getid_producto(){
		return productoId;
	}
	
	public void setUsuarioId(Long usuarioId){
		this.usuarioId = usuarioId;
	}
	public void setProductoId(Long productoId){
		this.productoId = productoId;
	}
}

