package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Usuario", indexes = { @Index(columnList = "id") })
public class Usuario implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "account_id_generator", sequenceName = "account_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_id_generator")
	private Long id;

	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String username;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private Date lastlog = new Date();
	
	@Column(nullable = false)
	private Long tipo;
	
	/* tipo 1 - > registro de venta
	 * tipo 2 -> registro producto
	 * tipo 3 -> admi
	 */

	public Usuario() {
	}

	public Usuario(String username, String password) {
		this.setUsername(username);
		this.password = password;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getLoged() {
		return lastlog;
	}

	public void setLoged(Date lastlog) {
		this.lastlog = lastlog;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public Long getTipo() {
		return tipo;
	}

	public void setTipo(Long tipo) {
		this.tipo = tipo;
	}

}
