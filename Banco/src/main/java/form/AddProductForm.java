package form;

public class AddProductForm {
	private String nombreProducto;
	private String tipo;
	private String clasificacion;
	private Long cantidad;
	
	public String getnombreProducto() {
		return nombreProducto;
	}

	public String getTipo() {
		return tipo;
	}
	
	public String getClasificacion() {
		return clasificacion;
	}

	public Long getCantidad() {
		return cantidad;
	}
	
	public void setnombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}
	
	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

}
