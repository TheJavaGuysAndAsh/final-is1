package form;

public class RegistroProveedorForm {
	private String nombreProveedor;
	private Long contacto;
	private String clasificacion;
	private String direccion;
	private Long productoId;
	
	public String getnombreProducto() {
		return nombreProveedor;
	}
	
	public String getDireccion() {
		return direccion;
	}

	public Long getContacto() {
		return contacto;
	}
	
	public String getClasificacion() {
		return clasificacion;
	}

	public Long getProductoId() {
		return productoId;
	}
	
	public void setnombreProducto(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}
	
	public void setContacto(Long contacto) {
		this.contacto = contacto;
	}
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}
	
	public void setProductoId(Long productoId) {
		this.productoId = productoId;
	}

}
