package form;
public class RegistroVentaForm {
	private Long usuarioId;
	private Long productoId;
	private Long cantidad;
	
	public Long getUsuarioId() {
		return usuarioId;
	}
	
	public Long getProductoId() {
		return productoId;
	}

	public Long getCantidad() {
		return cantidad;
	}
	
	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}
	
	public void setProductoId(Long productoId) {
		this.productoId = productoId;
	}
	
	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

}
