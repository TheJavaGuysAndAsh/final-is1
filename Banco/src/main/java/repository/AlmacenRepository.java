package repository;

import java.util.Collection;

import domain.Almacen;

public interface AlmacenRepository extends BaseRepository<Almacen, Long> {
	Almacen findByName(String nombre_almacen);
	Almacen findById(Long id_almacen);
	Almacen Register(String nombre_almacen, String localizacion);
}