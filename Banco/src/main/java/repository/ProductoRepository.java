package repository;

import java.util.Collection;

import domain.Producto;

public interface ProductoRepository extends BaseRepository<Producto, Long> {
	Producto findByNombre(String nombreProducto);
	Producto findById(Long id_producto);
	Producto agregarProducto(String nombreProducto,String clasificacion ,String tipo ,Long cantidad);
}