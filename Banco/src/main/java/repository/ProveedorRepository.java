package repository;
//import java.util.Collection;
import domain.Proveedor;
public interface ProveedorRepository extends BaseRepository<Proveedor, Long> {
	Proveedor findById_Producto(String Id_producto);
}