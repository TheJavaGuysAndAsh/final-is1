package repository;

import domain.Reg_pedido;

public interface RegpedidoRepository extends BaseRepository<Reg_pedido, Long> {
	Reg_pedido findByProv(Long proveedor_id);
	Reg_pedido findById(Long id_reg);
	Reg_pedido Ingresar(Long cantidad,Boolean estado,Long id_producto,Long proveedor_id);
}

