package repository;

import java.util.Collection;

import domain.Usuario;

public interface UsuarioRepository extends BaseRepository<Usuario, Long> {
	Usuario findByNumber(String number);
	Usuario findByUsername(String username, String password);
	Usuario IngresarByNumber(String password, String username);
}

