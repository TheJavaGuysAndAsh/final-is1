package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.AlmacenRepository;
import domain.Almacen;

@Repository
public class JpaAlmacenRepository extends JpaBaseRepository<Almacen, Long> implements 
	AlmacenRepository{

	@Override
	public Almacen findByName(String nombre_almacen) {
		String jpaQuery = "SELECT a FROM almacen a WHERE a.name = :nombre_almacen";
		TypedQuery<Almacen> query = entityManager.createQuery(jpaQuery, Almacen.class);
		query.setParameter("name", nombre_almacen);
		return getFirstResult(query);
	}
	@Override
	public Almacen findById(Long id_producto) {
		String jpaQuery = "SELECT a FROM producto a WHERE a.id_producto = :id_producto";
		TypedQuery<Almacen> query = entityManager.createQuery(jpaQuery, Almacen.class);
		query.setParameter("id_producto", id_producto);
		return getFirstResult(query);
	}

	@Override
	public Almacen Register(String nombre_almacen,String localizacion) {
		Almacen A=new Almacen();
		persist(A);
		return A;
	}
	
	

}
