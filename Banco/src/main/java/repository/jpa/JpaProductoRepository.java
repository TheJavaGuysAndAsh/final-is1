package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.ProductoRepository;
import domain.Producto;

@Repository
public class JpaProductoRepository extends JpaBaseRepository<Producto, Long> implements
	ProductoRepository {

	@Override
	public Producto findByNombre(String nombreProducto) {
		String jpaQuery = "SELECT a FROM Producto a WHERE a.nombreProducto = :nombreProducto";
		TypedQuery<Producto> query = entityManager.createQuery(jpaQuery, Producto.class);
		query.setParameter("nombreProducto", nombreProducto);
		return getFirstResult(query);
	}
	
	@Override
	public Producto findById(Long id_producto) {
		String jpaQuery = "SELECT a FROM Producto a WHERE a.id_producto = :id_producto";
		TypedQuery<Producto> query = entityManager.createQuery(jpaQuery, Producto.class);
		query.setParameter("id_producto", id_producto);
		return getFirstResult(query);
	}

	@Override
	public Producto agregarProducto(String nombreProducto ,String clasificacion ,String tipo ,Long cantidad) {
		Producto nuevoProducto = new Producto(nombreProducto,nombreProducto,tipo,cantidad);
		persist(nuevoProducto);
		return nuevoProducto;
	}
}
