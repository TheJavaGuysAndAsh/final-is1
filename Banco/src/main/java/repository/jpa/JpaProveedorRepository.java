package repository.jpa;
//import java.util.Collection;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import repository.ProveedorRepository;
import domain.Proveedor;

@Repository
public class JpaProveedorRepository extends JpaBaseRepository<Proveedor, Long> implements
ProveedorRepository {


	@Override
	public Proveedor findById_Producto(String Id_producto) {
		//SELECT * FROM PROVEEDOR WHERE ID_PRODCUTO=...
		String jpaQuery = "SELECT a FROM Proveedor a WHERE a.id_producto = :id_producto";
		TypedQuery<Proveedor> query = entityManager.createQuery(jpaQuery, Proveedor.class);
		query.setParameter("id_producto", Id_producto);
		return getFirstResult(query);
	}
}