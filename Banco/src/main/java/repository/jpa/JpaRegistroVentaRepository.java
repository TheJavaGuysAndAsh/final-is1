package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.RegistroVentaRepository;
import domain.RegistroVenta;

@Repository
public class JpaRegistroVentaRepository extends JpaBaseRepository<RegistroVenta, Long> implements
	RegistroVentaRepository {


}
