package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.RegpedidoRepository;
import domain.Reg_pedido;

@Repository
public class JpaRegpedidoRepository extends JpaBaseRepository<Reg_pedido, Long> implements
	RegpedidoRepository {

	@Override
	public Reg_pedido findByProv(Long proveedor_id) {
		//SELECT a.id, a.number, a.date FROM tbl_account a WHERE a.number = :number
		String jpaQuery = "SELECT a FROM producto a WHERE a.proveedor_id = :proveedor_id";
		TypedQuery<Reg_pedido> query = entityManager.createQuery(jpaQuery, Reg_pedido.class);
		query.setParameter("proveedor_id", proveedor_id);
		return getFirstResult(query);
	}
	@Override
	public Reg_pedido findById(Long id_reg) {
		String jpaQuery = "SELECT a FROM producto a WHERE a.id_reg = :id_reg";
		TypedQuery<Reg_pedido> query = entityManager.createQuery(jpaQuery, Reg_pedido.class);
		query.setParameter("id_reg", id_reg);
		return getFirstResult(query);
	}

	@Override
	public Reg_pedido Ingresar(Long cantidad,Boolean estado,Long id_producto,Long proveedor_id) 
	{
		// INSERT INTO USURIO (password,username) VALUES ()
		Reg_pedido U=new Reg_pedido();
		persist(U);
		return U;
	}
	
	

}
