package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.UsuarioRepository;
import domain.Usuario;

@Repository
public class JpaUsuarioRepository extends JpaBaseRepository<Usuario, Long> implements
	UsuarioRepository {

	@Override
	public Usuario findByNumber(String number) {
		//SELECT a.id, a.number, a.date FROM tbl_account a WHERE a.number = :number
		String jpaQuery = "SELECT a FROM Usuario a WHERE a.number = :number";
		TypedQuery<Usuario> query = entityManager.createQuery(jpaQuery, Usuario.class);
		query.setParameter("number", number);
		return getFirstResult(query);
	}
	
	@Override
	public Usuario findByUsername(String username, String password) {
		//SELECT a.id, a.number, a.date FROM tbl_account a WHERE a.number = :number
		String jpaQuery = "SELECT a FROM Usuario a WHERE a.username = :username and a.password = :password";
		TypedQuery<Usuario> query = entityManager.createQuery(jpaQuery, Usuario.class);
		query.setParameter("username", username);
		query.setParameter("password", password);
		return getFirstResult(query);
	}
	
	@Override
	public Usuario IngresarByNumber(String password, String username) {
		// INSERT INTO USURIO (password,username) VALUES ()
		Usuario U=new Usuario(username,password);
		persist(U);
		return U;
	}
	
	

}
