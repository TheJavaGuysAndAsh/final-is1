package service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.AlmacenRepository;
import domain.Almacen;

@Service
public class AlmacenService {

	@Autowired
	AlmacenRepository almacenRepository;

	@Transactional
	public void save(Almacen almacen) {
		if (almacen.getId() == null) {
			almacenRepository.persist(almacen);
		} else {
			almacenRepository.merge(almacen);
		}
	}

	public Almacen get(Long id) {
		return almacenRepository.find(id);
	}

	public Collection<Almacen> getAll() {
		return almacenRepository.findAll();
	}
}
