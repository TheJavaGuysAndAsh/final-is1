package service;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.UsuarioRepository;
import domain.Usuario;

@Service
public class LoginService {

	@Autowired
	UsuarioRepository userRepository;
	
	@Transactional
	public Usuario login(String username, String password) {
		Usuario usuario = userRepository.findByUsername(username,password);
		if (usuario == null) {
			usuario = new Usuario();
			usuario.setUsername(username);
			usuario.setPassword(password);
			userRepository.persist(usuario);
			return null;
		}
		usuario.setLoged(new Date());
		userRepository.merge(usuario);
		return usuario;
	}
}
