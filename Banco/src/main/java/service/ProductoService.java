package service;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import repository.ProductoRepository;
import domain.Producto;

@Service
public class ProductoService {
	
	@Autowired
	ProductoRepository productoRepository;
	
	@Transactional
	public void add(String nombreProducto,String tipo,String clasificacion,Long cantidad) {
		Producto nuevoProducto = productoRepository.findByNombre(nombreProducto);
		if(nuevoProducto == null)
		{
			nuevoProducto = new Producto();
			nuevoProducto.setCantidad(cantidad);
			nuevoProducto.setClasificacion(clasificacion);
			nuevoProducto.setNombre(nombreProducto);
			nuevoProducto.setTipo(tipo);
			productoRepository.persist(nuevoProducto);
		}
		else 
		{
			nuevoProducto.setCantidad(cantidad);
			nuevoProducto.setClasificacion(clasificacion);
			nuevoProducto.setNombre(nombreProducto);
			nuevoProducto.setTipo(tipo);
			productoRepository.merge(nuevoProducto);			
		}
	}

	public Producto get(Long id) {
		return productoRepository.find(id);
	}

	public Collection<Producto> getAll() {
		return productoRepository.findAll();
	}
}
