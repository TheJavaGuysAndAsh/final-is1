package service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.ProveedorRepository;
import domain.Proveedor;

@Service
public class ProveedorService {

	@Autowired
	ProveedorRepository proveedorRepository;

	@Transactional
	public void saveProveedor(String nombreProveedor,Long contacto,String clasificacion,String direccion,Long productoId) {
		Proveedor proveedor = new Proveedor();
		proveedor.setnombre_proveedor(nombreProveedor);
		proveedor.setDireccion(direccion);
		proveedor.setId(productoId);
		proveedor.setContacto(contacto);
		proveedorRepository.persist(proveedor);
		
	}

	public Proveedor get(Long id) {
		return proveedorRepository.find(id);
	}

	public Collection<Proveedor> getAll() {
		return proveedorRepository.findAll();
	}
}
