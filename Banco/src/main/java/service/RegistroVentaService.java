package service;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.RegistroVentaRepository;
import domain.RegistroVenta;

@Service
public class RegistroVentaService {

	@Autowired
	RegistroVentaRepository regpedidoRepository;
	
	@Transactional
	public void saveVenta(Long usuarioId, Long productoId, Long cantidad) {
		RegistroVenta registroventa = new RegistroVenta();
		registroventa.setUsuarioId(usuarioId);
		registroventa.setProductoId(productoId);
		registroventa.setCantidad(cantidad);
		registroventa.setFechaVenta(new Date());
		regpedidoRepository.persist(registroventa);
	}

}
