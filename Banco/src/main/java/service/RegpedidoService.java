package service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.RegpedidoRepository;
import domain.Reg_pedido;

@Service
public class RegpedidoService {

	@Autowired
	RegpedidoRepository regpedidoRepository;

	@Transactional
	public void save(Reg_pedido reg_pedido) {
		if (reg_pedido.getId() == null) {
			regpedidoRepository.persist(reg_pedido);
		} else {
			regpedidoRepository.merge(reg_pedido);
		}
	}

	public Reg_pedido get(Long id) {
		return regpedidoRepository.find(id);
	}

	public Collection<Reg_pedido> getAll() {
		return regpedidoRepository.findAll();
	}
}
