package service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.UsuarioRepository;
import domain.Usuario;

@Service
public class UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Transactional
	public void save(Usuario usuario) {
		if (usuario.getId() == null) {
			usuarioRepository.persist(usuario);
		} else {
			usuarioRepository.merge(usuario);
		}
	}

	public Usuario get(Long id) {
		return usuarioRepository.find(id);
	}

	public Collection<Usuario> getAll() {
		return usuarioRepository.findAll();
	}
}
